# Live Currencies

Written in Kotlin.

### Implemented features
- Load/Update currencies rates each second
- Live rate calculation
- Loading state handling
- Some Unit test

### Used technologies
- MVVM with android architecture components (LiveData, Data binding, Navigation component)
- Retrofit - for handling REST API calls
- Kotlin coroutines - for handling async tasks
- Dagger - DI solution for code decoupling and increase testability
