package lt.mazajev.raimond.livecurrencies

import android.app.Application
import lt.mazajev.raimond.livecurrencies.di.AppComponent
import lt.mazajev.raimond.livecurrencies.di.DaggerAppComponent

open class LiveCurrenciesApp : Application() {

    val appComponent: AppComponent by lazy {
        initializeComponent()
    }

    open fun initializeComponent(): AppComponent = DaggerAppComponent.factory().create(applicationContext)
}