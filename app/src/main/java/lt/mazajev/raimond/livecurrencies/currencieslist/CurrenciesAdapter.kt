package lt.mazajev.raimond.livecurrencies.currencieslist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import lt.mazajev.raimond.livecurrencies.BR
import lt.mazajev.raimond.livecurrencies.databinding.CurrencyItemBinding
import lt.mazajev.raimond.livecurrencies.utils.BindableAdapter
import lt.mazajev.raimond.livecurrencies.utils.showKeyboard

class CurrenciesAdapter(private val viewModel: CurrenciesListViewModel, private val viewLifecycleOwner: LifecycleOwner) :
    ListAdapter<CurrencyItem, CurrencyViewHolder>(COMPARATOR),
    BindableAdapter<CurrencyItem> {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val viewBinding = CurrencyItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        viewBinding.lifecycleOwner = viewLifecycleOwner
        viewBinding.setVariable(BR.vm, viewModel)
        return CurrencyViewHolder(viewBinding, viewModel)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.binding.executePendingBindings()
    }

    companion object {
        val COMPARATOR = object : DiffUtil.ItemCallback<CurrencyItem>() {
            override fun areContentsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean = oldItem == newItem

            override fun areItemsTheSame(oldItem: CurrencyItem, newItem: CurrencyItem): Boolean =
                oldItem.currencyCode == newItem.currencyCode
        }
    }
}

class CurrencyViewHolder(val binding: CurrencyItemBinding, private val viewModel: CurrenciesListViewModel) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: CurrencyItem) {
        binding.value.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus -> if (hasFocus) viewModel.onCurrencySelected(item) }
        binding.root.setOnClickListener { binding.value.requestFocus() }

        if (item.isBaseItem) {
            binding.value.post {
                binding.value.showKeyboard()
                binding.value.setSelection(binding.value.text.length)
            }
        }

        binding.setVariable(BR.item, item)
    }
}

