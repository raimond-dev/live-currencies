package lt.mazajev.raimond.livecurrencies.currencieslist

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import lt.mazajev.raimond.livecurrencies.LiveCurrenciesApp
import lt.mazajev.raimond.livecurrencies.R
import lt.mazajev.raimond.livecurrencies.currencieslist.CurrenciesListViewModel.LoadingState.Failed
import lt.mazajev.raimond.livecurrencies.databinding.CurrenciesListFragmentBinding
import javax.inject.Inject

class CurrenciesListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<CurrenciesListViewModel> { viewModelFactory }

    private lateinit var binding: CurrenciesListFragmentBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity().application as LiveCurrenciesApp).appComponent.currenciesListComponent().create().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = CurrenciesListFragmentBinding.inflate(inflater, container, false)
            .apply {
                vm = viewModel
                lifecycleOwner = viewLifecycleOwner
            }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.currenciesList.adapter = CurrenciesAdapter(viewModel, viewLifecycleOwner)
        viewModel.loadingState.observe(viewLifecycleOwner, Observer { loadingState ->
            when (loadingState) {
                Failed -> Snackbar
                    .make(requireView(), R.string.server_error_message, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.retry_action_label) { viewModel.retryFetch() }
                    .show()
            }
        })
    }

}