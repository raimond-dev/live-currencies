package lt.mazajev.raimond.livecurrencies.currencieslist

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import lt.mazajev.raimond.livecurrencies.R
import lt.mazajev.raimond.livecurrencies.currencieslist.CurrenciesListViewModel.LoadingState.Failed
import lt.mazajev.raimond.livecurrencies.currencieslist.CurrenciesListViewModel.LoadingState.Idle
import lt.mazajev.raimond.livecurrencies.currencieslist.CurrenciesListViewModel.LoadingState.Loading
import lt.mazajev.raimond.livecurrencies.network.Result.Error
import lt.mazajev.raimond.livecurrencies.network.Result.Success
import lt.mazajev.raimond.livecurrencies.network.RevolutService
import lt.mazajev.raimond.livecurrencies.utils.ResourceProvider
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.ParseException
import java.util.*
import javax.inject.Inject

private const val TIMER_DELAY = 1000L
private const val DEFAULT_CURRENCY = "EUR"

class CurrenciesListViewModel @Inject constructor(
    private val revolutService: RevolutService,
    private val resourceProvider: ResourceProvider,
    private val ioDispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _currencyItems = MutableLiveData<List<CurrencyItem>>().apply { value = emptyList() }
    val currencyItems: LiveData<List<CurrencyItem>> = _currencyItems

    private val _loadingState = MutableLiveData<LoadingState>().apply { value = Idle }
    val loadingState: LiveData<LoadingState> = _loadingState

    private var _latestCurrencies: LatestCurrencies? = null

    private var _baseCurrency = CurrencyItem(
        currencyCode = DEFAULT_CURRENCY,
        value = BigDecimal.ONE,
        currencyDescription = resourceProvider.getCurrencyDescription(DEFAULT_CURRENCY),
        flagRes = getFlagDrawable(DEFAULT_CURRENCY),
        isBaseItem = true
    )

    init {
        initRatesUpdater()
    }

    fun retryFetch() {
        _loadingState.value = Idle
        initRatesUpdater()
    }

    fun onCurrencyValueChanged(text: CharSequence, item: CurrencyItem) {
        if (item.isBaseItem && text.toString() != _baseCurrency.value.toString()) {
            try {
                val formatter = DecimalFormat()
                formatter.isParseBigDecimal = true
                val newValue = if (text.isEmpty()) BigDecimal.ZERO else formatter.parse(text.toString()) as BigDecimal
                _currencyItems.value?.first { it -> it.isBaseItem }?.updateValue(newValue)
                updateCurrencies()
            } catch (e: ParseException) {
                // do nothing or send some analytics
            }
        }
    }

    fun onCurrencySelected(item: CurrencyItem) {
        // Don't allow to change base currency if last fetching failed
        if (item.currencyCode == _baseCurrency.currencyCode || _loadingState.value == Failed) return
        _currencyItems.value?.let { items ->
            val list = items.map { i ->
                when {
                    i.isBaseItem -> i.copy(isBaseItem = false, creationTime = Date().time)
                    i.currencyCode == item.currencyCode -> i.copy(isBaseItem = true, creationTime = Date().time)
                        .also { _baseCurrency = it }
                    else -> i.copy(creationTime = Date().time)
                }
            }.sortedBy { !it.isBaseItem }
            _currencyItems.value = list
        }
        fetchCurrencies()
    }

    private fun initRatesUpdater() = viewModelScope.launch {
        withContext(ioDispatcher) {
            while (_loadingState.value != Failed) {
                fetchCurrencies()
                delay(TIMER_DELAY)
            }
        }
    }

    @VisibleForTesting
    fun fetchCurrencies() {
        if (_loadingState.value == Failed || _loadingState.value == Loading) return

        viewModelScope.launch {
            _loadingState.value = Loading
            val result = withContext(ioDispatcher) { revolutService.getLatestCurrencies(_baseCurrency.currencyCode) }
            _loadingState.value = when (result) {
                is Success -> {
                    _latestCurrencies = result.data
                    _currencyItems.value?.takeIf { it.isNotEmpty() }
                        ?.let { updateCurrencies() }
                        ?: publishList()

                    Idle
                }
                is Error -> Failed
            }
        }
    }

    private fun updateCurrencies() {
        _currencyItems.value?.forEach { currencyItem ->
            _latestCurrencies?.rates?.get(currencyItem.currencyCode)
                ?.let { rate ->
                    if (!currencyItem.isBaseItem) {
                        val newValue = rate.multiply(_baseCurrency.value)
                        currencyItem.updateValue(newValue)
                    }
                }
        }
    }

    private fun publishList() = _latestCurrencies?.let { latestCurrencies ->
        val list = mutableListOf<CurrencyItem>()

        // base currency always should be first in the list
        _baseCurrency = CurrencyItem(
            currencyCode = latestCurrencies.baseCurrency,
            value = BigDecimal.ONE,
            currencyDescription = resourceProvider.getCurrencyDescription(latestCurrencies.baseCurrency),
            flagRes = getFlagDrawable(latestCurrencies.baseCurrency),
            isBaseItem = true
        )
        list.add(_baseCurrency)

        latestCurrencies.rates.forEach { (currencyCode, rate) ->
            list.add(
                CurrencyItem(
                    currencyCode = currencyCode,
                    value = rate * _baseCurrency.value,
                    currencyDescription = resourceProvider.getCurrencyDescription(currencyCode),
                    flagRes = getFlagDrawable(currencyCode),
                    isBaseItem = false
                )
            )
        }
        _currencyItems.value = list
    }

    sealed class LoadingState {
        object Idle : LoadingState()
        object Loading : LoadingState()
        object Failed : LoadingState()
    }

    // true only if it's first time data is fetched
    fun LoadingState.shouldShowLoading(): Boolean = this == Loading && _latestCurrencies == null

    companion object {
        private val flagResources = mapOf(
            "AUD" to R.drawable.au,
            "BGN" to R.drawable.bg,
            "BRL" to R.drawable.br,
            "CAD" to R.drawable.ca,
            "CHF" to R.drawable.ch,
            "CNY" to R.drawable.cn,
            "CZK" to R.drawable.cz,
            "DKK" to R.drawable.dk,
            "EUR" to R.drawable.eu,
            "GBP" to R.drawable.uk,
            "HKD" to R.drawable.hk,
            "HRK" to R.drawable.hr,
            "HUF" to R.drawable.hu,
            "IDR" to R.drawable.id,
            "ILS" to R.drawable.il,
            "INR" to R.drawable.in_f,
            "ISK" to R.drawable.is_f,
            "JPY" to R.drawable.jp,
            "KRW" to R.drawable.kr,
            "MXN" to R.drawable.mx,
            "MYR" to R.drawable.my,
            "NOK" to R.drawable.no,
            "NZD" to R.drawable.nz,
            "PHP" to R.drawable.ph,
            "PLN" to R.drawable.pl,
            "RON" to R.drawable.ro,
            "RUB" to R.drawable.ru,
            "SEK" to R.drawable.se,
            "SGD" to R.drawable.sg,
            "THB" to R.drawable.th,
            "USD" to R.drawable.us,
            "ZAR" to R.drawable.za
        )

        private fun getFlagDrawable(currencyCode: String): Int = flagResources[currencyCode] ?: R.drawable.unknown_flag
    }
}
