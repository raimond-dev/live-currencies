package lt.mazajev.raimond.livecurrencies.currencieslist

import androidx.annotation.DrawableRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import java.math.BigDecimal
import java.util.*

data class CurrencyItem(
    val currencyCode: String,
    val currencyDescription: String,
    val isBaseItem: Boolean,
    var value: BigDecimal,
    @DrawableRes
    val flagRes: Int,
    // This field is used only so RecycleView adapter would understand that we passed new object after reordering.
    val creationTime: Long = Date().time
) {
    private val _value = MutableLiveData(value)
    val liveValue: LiveData<BigDecimal> = _value
    fun updateValue(newValue: BigDecimal) {
        value = newValue
        _value.value = newValue
    }
}