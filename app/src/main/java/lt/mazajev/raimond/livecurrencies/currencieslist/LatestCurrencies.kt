package lt.mazajev.raimond.livecurrencies.currencieslist

import java.math.BigDecimal

data class LatestCurrencies(
    val baseCurrency: String,
    val rates: Map<String, BigDecimal>
)