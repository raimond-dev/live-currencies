package lt.mazajev.raimond.livecurrencies.currencieslist.di

import dagger.Subcomponent
import lt.mazajev.raimond.livecurrencies.currencieslist.CurrenciesListFragment

@Subcomponent(modules = [CurrenciesListModule::class])
interface CurrenciesListComponent {
    @Subcomponent.Factory
    interface Factory {
        fun create(): CurrenciesListComponent
    }

    fun inject(fragment: CurrenciesListFragment)
}