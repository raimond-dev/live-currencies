package lt.mazajev.raimond.livecurrencies.currencieslist.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import lt.mazajev.raimond.livecurrencies.currencieslist.CurrenciesListViewModel
import lt.mazajev.raimond.livecurrencies.di.ViewModelKey

@Module
abstract class CurrenciesListModule {
    @Binds
    @IntoMap
    @ViewModelKey(CurrenciesListViewModel::class)
    abstract fun bindCurrenciesListViewModel(viewModel: CurrenciesListViewModel): ViewModel
}