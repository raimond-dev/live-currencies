package lt.mazajev.raimond.livecurrencies.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import lt.mazajev.raimond.livecurrencies.currencieslist.di.CurrenciesListComponent
import lt.mazajev.raimond.livecurrencies.network.NetworkModule
import lt.mazajev.raimond.livecurrencies.utils.UtilsModule

@ApplicationScope
@Component(
    modules = [
        AppModule::class,
        UtilsModule::class,
        NetworkModule::class,
        ViewModelBuilderModule::class,
        SubcomponentsModule::class
    ]
)
interface AppComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): AppComponent
    }

    fun currenciesListComponent(): CurrenciesListComponent.Factory
}

@Module(
    subcomponents = [
        CurrenciesListComponent::class
    ]
)
object SubcomponentsModule