package lt.mazajev.raimond.livecurrencies.di

import dagger.Module
import dagger.Provides
import kotlinx.coroutines.Dispatchers

@Module
object AppModule {
    @JvmStatic
    @ApplicationScope
    @Provides
    fun provideIoDispatcher() = Dispatchers.IO
}