package lt.mazajev.raimond.livecurrencies.di

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention
annotation class ApplicationScope