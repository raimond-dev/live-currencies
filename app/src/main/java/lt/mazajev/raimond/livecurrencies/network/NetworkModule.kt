package lt.mazajev.raimond.livecurrencies.network

import dagger.Binds
import dagger.Module
import dagger.Provides
import lt.mazajev.raimond.livecurrencies.network.NetworkModule.Bindings
import lt.mazajev.raimond.livecurrencies.di.ApplicationScope
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val API_URL = "https://hiring.revolut.codes/api/android/"

@Module(includes = [Bindings::class])
class NetworkModule {

    @ApplicationScope
    @Provides
    fun provideRevolutRetrofitService(): RevolutRetrofitService = Retrofit.Builder()
        .baseUrl(API_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(RevolutRetrofitService::class.java)


    @Module
    interface Bindings {
        @ApplicationScope
        @Binds
        fun bindRevolutService(revolutService: RevolutServiceImpl): RevolutService
    }
}