package lt.mazajev.raimond.livecurrencies.network

import lt.mazajev.raimond.livecurrencies.network.models.LatestCurrenciesResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutRetrofitService {

    @GET("latest")
    suspend fun getLatestCurrencies(@Query("base") baseCurrency: String): LatestCurrenciesResponse

}