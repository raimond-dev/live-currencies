package lt.mazajev.raimond.livecurrencies.network

import lt.mazajev.raimond.livecurrencies.currencieslist.LatestCurrencies
import lt.mazajev.raimond.livecurrencies.network.models.Response
import javax.inject.Inject


interface RevolutService {
    suspend fun getLatestCurrencies(baseCurrency: String): Result<LatestCurrencies>
}

class RevolutServiceImpl @Inject constructor(private val revolutRetrofitService: RevolutRetrofitService) : RevolutService {
    override suspend fun getLatestCurrencies(baseCurrency: String): Result<LatestCurrencies> =
        doRequest { revolutRetrofitService.getLatestCurrencies(baseCurrency) }
}

private inline fun <reified R : Any, T : Any> doRequest(call: () -> T): R = try {
    when (val res = call()) {
        is Response<*> -> Result.Success(res.toDomain()) as R
        else -> Result.Success(res) as R
    }
} catch (e: Exception) {
    Result.Error(e) as R
}