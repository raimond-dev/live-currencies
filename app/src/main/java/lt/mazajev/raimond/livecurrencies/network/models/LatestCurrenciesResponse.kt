package lt.mazajev.raimond.livecurrencies.network.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import lt.mazajev.raimond.livecurrencies.currencieslist.LatestCurrencies
import java.math.BigDecimal
import java.text.DecimalFormat

@JsonClass(generateAdapter = true)
data class LatestCurrenciesResponse(
    @Json(name = "baseCurrency")
    val baseCurrency: String,
    @Json(name = "rates")
    val rates: Map<String, String>
) : Response<LatestCurrencies> {

    override fun toDomain(): LatestCurrencies {
        val formatter = DecimalFormat()
        formatter.isParseBigDecimal = true

        return LatestCurrencies(
            baseCurrency = baseCurrency,
            rates = rates.mapValues { (_, v) -> formatter.parse(v) as BigDecimal }
        )
    }

}