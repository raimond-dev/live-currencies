package lt.mazajev.raimond.livecurrencies.network.models

interface Response<T : Any> {
    fun toDomain(): T
}