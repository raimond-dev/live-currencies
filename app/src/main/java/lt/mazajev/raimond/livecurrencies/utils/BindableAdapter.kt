package lt.mazajev.raimond.livecurrencies.utils

interface BindableAdapter<T> {
    fun submitList(data: List<T>?)
}