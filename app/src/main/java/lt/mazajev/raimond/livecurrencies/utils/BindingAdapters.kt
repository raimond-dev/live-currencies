package lt.mazajev.raimond.livecurrencies.utils

import android.widget.EditText
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import java.math.BigDecimal
import java.text.DecimalFormat

@BindingAdapter("data")
fun <T> setRecyclerData(recyclerView: RecyclerView, items: List<T>?) {
    if (items == null) return
    if (recyclerView.adapter is BindableAdapter<*>) {
        (recyclerView.adapter as BindableAdapter<T>).submitList(items)
    }
}

@BindingAdapter("src")
fun setSource(view: ImageView, @DrawableRes backgroundRes: Int) {
    if (backgroundRes == 0) return
    view.background = view.context.getDrawable(backgroundRes)
}

@BindingAdapter("currencyValue")
fun setCurrencyValue(view: EditText, value: BigDecimal?) {
    if (value == null || view.hasFocus()) return
    val formatter = DecimalFormat()
    formatter.isParseBigDecimal = true
    val parsed = formatter.format(value)
    view.setText(parsed)
}