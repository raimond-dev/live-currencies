package lt.mazajev.raimond.livecurrencies.utils

import android.content.Context
import lt.mazajev.raimond.livecurrencies.R
import javax.inject.Inject

interface ResourceProvider {
    fun getCurrencyDescription(code: String): String
}

class ResourceProviderImpl @Inject constructor(private val context: Context) : ResourceProvider {

    override fun getCurrencyDescription(code: String): String {
        val currencyStrings = context.resources.getStringArray(R.array.currency_codes)
        return currencyStrings.indexOf(code).takeIf { it > -1 }
            ?.let { index ->
                val currencyDescriptions = context.resources.getStringArray(R.array.currency_descriptions)
                currencyDescriptions[index]
            }
            ?: ""
    }
}