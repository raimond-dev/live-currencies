package lt.mazajev.raimond.livecurrencies.utils

import dagger.Binds
import dagger.Module
import lt.mazajev.raimond.livecurrencies.di.ApplicationScope

@Module
interface UtilsModule {
    @ApplicationScope
    @Binds
    fun bindResourceProvider(res: ResourceProviderImpl): ResourceProvider
}