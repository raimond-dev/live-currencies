package lt.mazajev.raimond.livecurrencies.currencieslist

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.TestObserver
import com.jraska.livedata.test
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import lt.mazajev.raimond.livecurrencies.MainCoroutineRule
import lt.mazajev.raimond.livecurrencies.currencieslist.CurrenciesListViewModel.LoadingState
import lt.mazajev.raimond.livecurrencies.currencieslist.CurrenciesListViewModel.LoadingState.Failed
import lt.mazajev.raimond.livecurrencies.currencieslist.CurrenciesListViewModel.LoadingState.Idle
import lt.mazajev.raimond.livecurrencies.currencieslist.CurrenciesListViewModel.LoadingState.Loading
import lt.mazajev.raimond.livecurrencies.network.Result
import lt.mazajev.raimond.livecurrencies.network.RevolutService
import lt.mazajev.raimond.livecurrencies.utils.ResourceProvider
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.math.BigDecimal

@ExperimentalCoroutinesApi
class CurrenciesListViewModelTest {

    @get:Rule
    var liveDataTestRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val revolutService = mockk<RevolutService>()
    private val resourceProvider = mockk<ResourceProvider>()
    private val defaultBaseCurrency = "EUR"
    private val fakeCurrencies = LatestCurrencies(
        baseCurrency = defaultBaseCurrency,
        rates = mapOf(
            "AUD" to BigDecimal(0.152),
            "BGN" to BigDecimal(0.189),
            "BRL" to BigDecimal(0.399)
        )
    )
    private val fakeCurrenciesResult = Result.Success(fakeCurrencies)
    private val fakeCurrencyDescription = "fake"

    private lateinit var viewModel: CurrenciesListViewModel
    private lateinit var testCurrenciesListObserver: TestObserver<List<CurrencyItem>>
    private lateinit var testStateObserver: TestObserver<LoadingState>

    @Before
    fun init() {
        coEvery { revolutService.getLatestCurrencies(any()) } returns fakeCurrenciesResult
        every { resourceProvider.getCurrencyDescription(any()) } returns fakeCurrencyDescription
    }

    @Test
    fun `should fetch currencies - on init`() {
        initViewModel()
        coVerify { revolutService.getLatestCurrencies(any()) }
    }

    @Test
    fun `loading state should change - when request is made`() {
        initViewModel()
        viewModel.fetchCurrencies()
        testStateObserver.assertValueHistory(
            Idle,
            Loading,
            Idle
        )
    }

    @Test
    fun `loading state should be Failed - when on request error`() {
        coEvery { revolutService.getLatestCurrencies(any()) } returns Result.Error(Exception(""))
        initViewModel()
        testStateObserver.assertValue { it == Failed }
    }

    @Test
    fun `currency list should be published - when data fetched`() {
        initViewModel()
        viewModel.fetchCurrencies()
        testCurrenciesListObserver.assertValue { it.isNotEmpty() && it[1].currencyCode == "AUD" }
    }

    @Test
    fun `CurrencyItem value is updated - when baseCurrency value changes`() {
        initViewModel()
        val fakeItem = CurrencyItem(
            currencyCode = defaultBaseCurrency,
            currencyDescription = "",
            isBaseItem = true,
            value = BigDecimal.ONE,
            flagRes = 0
        )
        val newValue = "10"
        viewModel.onCurrencyValueChanged(newValue, fakeItem)
        testCurrenciesListObserver.assertValue { it[0].value == BigDecimal(newValue) }
        testCurrenciesListObserver.assertValue { it[1].value == BigDecimal(newValue) * fakeCurrencies.rates["AUD"]!! }
        testCurrenciesListObserver.assertValue { it[2].value == BigDecimal(newValue) * fakeCurrencies.rates["BGN"]!! }
        testCurrenciesListObserver.assertValue { it[3].value == BigDecimal(newValue) * fakeCurrencies.rates["BRL"]!! }
    }

    @Test
    fun `correct base currency should be selected - when user selects new base currency`() {
        initViewModel()
        val fakeItem = CurrencyItem(
            currencyCode = "AUD",
            currencyDescription = "",
            value = BigDecimal.ONE,
            isBaseItem = false,
            flagRes = 0
        )
        viewModel.onCurrencySelected(fakeItem)
        testCurrenciesListObserver.assertValue { it[0].currencyCode == fakeItem.currencyCode }
    }

    @Test
    fun `base currency should not be selected - if last fetch request failed`() {
        coEvery { revolutService.getLatestCurrencies(any()) } returns fakeCurrenciesResult andThen Result.Error(Exception(""))
        initViewModel()
        viewModel.fetchCurrencies()
        val fakeItem = CurrencyItem(
            currencyCode = "AUD",
            currencyDescription = "",
            value = BigDecimal.ONE,
            isBaseItem = false,
            flagRes = 0
        )
        viewModel.onCurrencySelected(fakeItem)
        testStateObserver.assertValue { it == Failed }
        testCurrenciesListObserver.assertValue { it[0].currencyCode == defaultBaseCurrency }
    }

    private fun initViewModel() {
        viewModel = CurrenciesListViewModel(
            revolutService = revolutService,
            resourceProvider = resourceProvider,
            ioDispatcher = Dispatchers.Unconfined
        ).apply {
            testStateObserver = loadingState.test()
            testCurrenciesListObserver = currencyItems.test()
        }
    }
}