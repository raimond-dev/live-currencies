package lt.mazajev.raimond.livecurrencies.network

import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import lt.mazajev.raimond.livecurrencies.MainCoroutineRule
import lt.mazajev.raimond.livecurrencies.network.Result.Error
import lt.mazajev.raimond.livecurrencies.network.Result.Success
import lt.mazajev.raimond.livecurrencies.network.models.LatestCurrenciesResponse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.math.BigDecimal

@ExperimentalCoroutinesApi
class RevolutServiceTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var revolutService: RevolutService
    private val revolutRetrofitService = mockk<RevolutRetrofitService>()
    private val fakeCurrenciesResponse = LatestCurrenciesResponse(
        baseCurrency = "EUR",
        rates = mapOf(
            "AUD" to "0.152",
            "BGN" to "0.189",
            "BRL" to "0.399"
        )
    )

    private val fakeBadCurrenciesResponse = LatestCurrenciesResponse(
        baseCurrency = "EUR",
        rates = mapOf(
            "AUD" to "@@"
        )
    )

    @Before
    fun init() {
        revolutService = RevolutServiceImpl(revolutRetrofitService)
    }

    @Test
    fun `should return success - when service is called`() {
        coEvery { revolutRetrofitService.getLatestCurrencies(any()) } returns fakeCurrenciesResponse
        mainCoroutineRule.runBlockingTest {
            val res = revolutService.getLatestCurrencies("EUR")
            assertTrue(res is Success)
        }
    }

    @Test
    fun `should return converted data - when service is called`() {
        coEvery { revolutRetrofitService.getLatestCurrencies(any()) } returns fakeCurrenciesResponse
        mainCoroutineRule.runBlockingTest {
            val res = revolutService.getLatestCurrencies("EUR")
            assertTrue((res as Success).data.rates["AUD"] is BigDecimal)
        }
    }

    @Test
    fun `should return error - when service returns bad data`() {
        coEvery { revolutRetrofitService.getLatestCurrencies(any()) } returns fakeBadCurrenciesResponse
        mainCoroutineRule.runBlockingTest {
            val res = revolutService.getLatestCurrencies("EUR")
            assertTrue(res is Error)
        }
    }
}