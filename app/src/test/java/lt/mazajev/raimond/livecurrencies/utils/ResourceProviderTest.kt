package lt.mazajev.raimond.livecurrencies.utils

import android.content.Context
import android.content.res.Resources
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class ResourceProviderTest {
    private lateinit var resourceProvider: ResourceProvider
    private val context = mockk<Context>()
    private val resources = mockk<Resources>()

    @Before
    fun init() {
        every { context.resources } returns resources
        every { resources.getStringArray(any()) } returns emptyArray()
        resourceProvider = ResourceProviderImpl(context)
    }

    @Test
    fun `should return empty string - if no currency description found`() {
        val res = resourceProvider.getCurrencyDescription("test")
        assertTrue(res == "")
    }

    @Test
    fun `should return correct string - if description exists`() {
        every { resources.getStringArray(any()) } returns arrayOf("test")
        val res = resourceProvider.getCurrencyDescription("test")
        assertTrue(res == "test")
    }
}